package ru.aushakov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.bootstrap.Bootstrap;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.ConfigProperty;

import java.io.File;

public final class Backuper extends Thread {

    @NotNull
    private final Bootstrap bootstrap;

    public Backuper(@NotNull final Bootstrap bootstrap) {
        setDaemon(true);
        this.bootstrap = bootstrap;
    }

    private void load() {
        @NotNull final String fileXml = bootstrap.getDataService().getFileBackup();
        @NotNull final File backup = new File(fileXml);
        if (backup.exists()) {
            bootstrap.parseCommand(TerminalConst.DATA_LOAD_BACKUP);
        } else {
            bootstrap.initData();
        }
    }

    public void init() {
        load();
        start();
    }

    @Override
    @SneakyThrows
    public void run() {
        @NotNull final Integer sleepTime =
                bootstrap.getPropertyService().getIntProperty(ConfigProperty.BACKUP_SLEEPTIME);
        while (true) {
            bootstrap.parseCommand(TerminalConst.DATA_SAVE_BACKUP);
            Thread.sleep(sleepTime);
        }
    }

}
