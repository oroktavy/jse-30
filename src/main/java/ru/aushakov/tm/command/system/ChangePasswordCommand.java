package ru.aushakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.api.service.IAuthService;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;
import ru.aushakov.tm.enumerated.Role;
import ru.aushakov.tm.util.TerminalUtil;

public final class ChangePasswordCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_CHANGE_PASSWORD;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change your current password";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER OLD PASSWORD:");
        @Nullable final String oldPassword = TerminalUtil.nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        authService.changePassword(oldPassword, newPassword);
        authService.logout();
    }

}
