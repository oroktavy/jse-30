package ru.aushakov.tm.command.system;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.command.AbstractCommand;
import ru.aushakov.tm.constant.TerminalConst;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.CMD_ARGUMENTS;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show application arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENT LIST]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (StringUtils.isEmpty(argument)) continue;
            System.out.println(argument);
        }
    }

}
